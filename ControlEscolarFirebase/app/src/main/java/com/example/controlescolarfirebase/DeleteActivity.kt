package com.example.controlescolarfirebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.controlescolarfirebase.databinding.ActivityDeleteBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class DeleteActivity : AppCompatActivity() {

    private lateinit var binding : ActivityDeleteBinding
    private  var reference : DatabaseReference? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDeleteBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btnEliminar.setOnClickListener {
            val noControl : String = binding.edtEliminar.getText().toString().toUpperCase()

            if(!noControl.isEmpty()){
                deleteData(noControl)
            }else{
                Toast.makeText(this, "Ingrese un número de control válido", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun deleteData(noControl: String) {
        reference = FirebaseDatabase.getInstance().getReference("Alumnos")
        reference!!.child(noControl).removeValue().addOnCompleteListener {
            if(it.isSuccessful){
                Toast.makeText(this,"El alumno fue eliminado", Toast.LENGTH_SHORT).show()
                binding.edtEliminar.text.clear()
            }else{
                Toast.makeText(this,"Ocurrio un fallo", Toast.LENGTH_SHORT).show()
            }
        }

    }
}