package com.example.controlescolarfirebase

import android.content.Intent
import android.icu.text.Transliterator
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.core.view.get
import com.example.controlescolarfirebase.databinding.ActivityMainBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class InsertActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var database : DatabaseReference


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val spinner = findViewById<Spinner>(R.id.cmbCarrera)
        val lista = resources.getStringArray(R.array.lista)
        val adatador = ArrayAdapter(this,android.R.layout.simple_spinner_item,lista)
        spinner.adapter = adatador





        binding.btnRegistrar.setOnClickListener{

            val numeroControl = binding.edtNoControl.text.toString().toUpperCase()
            val nombre = binding.edtNombre.text.toString().toUpperCase()
            val apellidoPaterno = binding.edtApellidoP.text.toString().toUpperCase()
            val apellidoMaterno = binding.edtApellidoM.text.toString().toUpperCase()
            val carrera = binding.cmbCarrera.selectedItem.toString().toUpperCase()



            database = FirebaseDatabase.getInstance().getReference("Alumnos")
            val alumno = Alumno(numeroControl,nombre,apellidoPaterno,apellidoMaterno,carrera)
            database.child(numeroControl).setValue(alumno).addOnSuccessListener {

                binding.edtNombre.text.clear()
                binding.edtApellidoP.text.clear()
                binding.edtApellidoM.text.clear()
                binding.edtNoControl.text.clear()
                binding.cmbCarrera.setSelection(0)

                Toast.makeText(this,"Se guardo correctamente", Toast.LENGTH_SHORT).show()
            }.addOnFailureListener {
                Toast.makeText(this,"No se guardo la información", Toast.LENGTH_SHORT).show()
            }

        }

        binding.btnConsultarDatos.setOnClickListener {
            val intent = Intent(this, ReadActivity::class.java)
            startActivity(intent)
        }
        binding.btnActualizarDatos.setOnClickListener {
            val intent = Intent(this, UpdateActivity::class.java)
            startActivity(intent)
        }
        binding.btnEliminarDatos.setOnClickListener {
            val intent = Intent(this, DeleteActivity::class.java)
            startActivity(intent)
        }






    }
}