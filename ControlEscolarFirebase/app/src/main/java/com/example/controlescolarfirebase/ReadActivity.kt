package com.example.controlescolarfirebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.controlescolarfirebase.databinding.ActivityReadBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class ReadActivity : AppCompatActivity() {

    private lateinit var binding : ActivityReadBinding
    private lateinit var database : DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReadBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnRead.setOnClickListener {
            val numeroControl: String = binding.edtNumeroControl.text.toString().toUpperCase()

            if(numeroControl.isNotEmpty()){
                readData(numeroControl)
            }else{
                Toast.makeText(this,"Ingrese un numero de control correcto",Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun readData(numeroControl: String) {

        database = FirebaseDatabase.getInstance().getReference("Alumnos")
        database.child(numeroControl).get().addOnSuccessListener {
            if (it.exists()){
                val noControl = it.child("numeroControl").value
                val nombre = it.child("nombre").value
                val apellidoP = it.child("apellidoPaterno").value
                val apellidoM = it.child("apellidoMaterno").value
                val carrera = it.child("carrera").value


                binding.edtNumeroControl.text.clear()
                binding.tvNumeroControl.text = noControl.toString()
                binding.tvNombre.text = nombre.toString()
                binding.tvApellidoPaterno.text = apellidoP.toString()
                binding.tvApellidoMaterno.text = apellidoM.toString()
                binding.tvCarrera.text = carrera.toString()

            }else{
                Toast.makeText(this,"El alumno no existe",Toast.LENGTH_SHORT).show()
            }
        }.addOnFailureListener {
            Toast.makeText(this,"Fallo!!!",Toast.LENGTH_SHORT).show()
        }

    }
}