package com.example.controlescolarfirebase

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.example.controlescolarfirebase.databinding.ActivityUpdateBinding
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class UpdateActivity : AppCompatActivity() {

    private lateinit var binding: ActivityUpdateBinding
    private lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val spinner = findViewById<Spinner>(R.id.cmbCarrera)
        val lista = resources.getStringArray(R.array.lista)
        val adatador = ArrayAdapter(this,android.R.layout.simple_spinner_item,lista)
        spinner.adapter = adatador

        binding.btnActualizar.setOnClickListener {

            val noControl = binding.edtNoControl.text.toString().toUpperCase()
            val nombre = binding.edtNombre.text.toString().toUpperCase()
            val apellidoP = binding.edtApellidoP.text.toString().toUpperCase()
            val apellidoM = binding.edtApellidoM.text.toString().toUpperCase()
            val carrera = binding.cmbCarrera.selectedItem.toString().toUpperCase()

            updateData(noControl,nombre,apellidoP,apellidoM,carrera)
        }
    }

    private fun updateData(noControl: String, nombre: String, apellidoP: String, apellidoM: String, carrera: String){

        database = FirebaseDatabase.getInstance().getReference("Alumnos")

        val user = mapOf<String,String>(
            "numeroControl" to noControl,
            "nombre" to nombre,
            "apellidoPaterno" to apellidoP,
            "apellidoPMaterno" to apellidoM,
            "carrera" to carrera,
        )

        database.child(noControl).updateChildren(user).addOnCompleteListener {
            binding.edtNoControl.text.clear()
            binding.edtNombre.text.clear()
            binding.edtApellidoP.text.clear()
            binding.edtApellidoM.text.clear()
            binding.cmbCarrera.setSelection(0)

            Toast.makeText(this, "Se actualizo correctamente", Toast.LENGTH_SHORT).show()
        }.addOnFailureListener {
            Toast.makeText(this, "No se actualizo ", Toast.LENGTH_SHORT).show()
        }
    }
}