package com.example.controlescolarfirebase

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insert)

        var handler = Handler().postDelayed({
            startActivity(Intent(this,InsertActivity::class.java))
        },5000)

    }
}